const fs = require('fs')                             //
fs.readFile('./text.txt', 'utf-8', (error, data) =>{ // используем метод, который работает в 
    if(!error){                                      // неблокирующем стиле (блокирующий вид: fs.readFileSync)
        console.log(data)                            //
    }                                                //
})                                                   //
console.log('Text: ')                          // выполняется до завершения чтения из файла